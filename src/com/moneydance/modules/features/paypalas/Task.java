/************************************************************\
 *      Copyright (C) 2008 Reilly Technologies, L.L.C.      *
\************************************************************/

package com.moneydance.modules.features.paypalas;

public interface Task {

  public void performTask() throws Throwable;

}
